:: Uncomment first line to reset database
c:\xampp\mysql\bin\mysql -h localhost -u root -e "DROP DATABASE IF EXISTS cmps490db;"

:: Creating database
c:\xampp\mysql\bin\mysql -h localhost -u root -e "CREATE DATABASE IF NOT EXISTS cmps490db;"
c:\xampp\mysql\bin\mysql -h localhost -u root -e "USE cmps490db; CREATE TABLE IF NOT EXISTS Users(UserId INT PRIMARY KEY AUTO_INCREMENT, UserName VARCHAR(20) UNIQUE, Password VARCHAR(20) UNIQUE, FName VARCHAR(20), LName VARCHAR(20));"
c:\xampp\mysql\bin\mysql -h localhost -u root -e "USE cmps490db; CREATE TABLE IF NOT EXISTS Complaints(ComplaintId INT PRIMARY KEY AUTO_INCREMENT,Description TEXT,UserId INT, FOREIGN KEY (UserId) REFERENCES Users(UserId), Status Text);"
c:\xampp\mysql\bin\mysql -h localhost -u root -e "USE cmps490db; CREATE TABLE IF NOT EXISTS Workers(WorkerId INT KEY AUTO_INCREMENT,UserName VARCHAR(20) UNIQUE, Password VARCHAR(20) UNIQUE, FName VARCHAR(20), LName VARCHAR(20));"

:: Inserting data
c:\xampp\mysql\bin\mysql -h localhost -u root -e "USE cmps490db; INSERT INTO Users (UserName, Password, FName, LName) VALUES('user1', 'pass1', 'John', 'Smith');"
c:\xampp\mysql\bin\mysql -h localhost -u root -e "USE cmps490db; INSERT INTO Users (UserName, Password, FName, LName) VALUES('user2', 'pass2', 'Mary', 'Jane');"
c:\xampp\mysql\bin\mysql -h localhost -u root -e "USE cmps490db; INSERT INTO Users (UserName, Password, FName, LName) VALUES('user3', 'pass3', 'James', 'Roy');"
c:\xampp\mysql\bin\mysql -h localhost -u root -e "USE cmps490db; INSERT INTO Complaints (Description, UserId, Status) VALUES('test1 description here', 1, 'active');"
c:\xampp\mysql\bin\mysql -h localhost -u root -e "USE cmps490db; INSERT INTO Complaints (Description, UserId, Status) VALUES('test2 description here', 2, 'completed');"
c:\xampp\mysql\bin\mysql -h localhost -u root -e "USE cmps490db; INSERT INTO Workers (UserName, Password, FName, LName) VALUES('worker1', 'pass1', 'James', 'Franklin');"
c:\xampp\mysql\bin\mysql -h localhost -u root -e "USE cmps490db; INSERT INTO Workers (UserName, Password, FName, LName) VALUES('worker2', 'pass2', 'Allen', 'Lin');"